### DESAFIO ###

Para o desafio utilizei
- ReacJS
- Redux
- Redux thunk
- React router
- Axios
- Material UI
- Lodash
- Recompose
- ASYNC/AWAIT

### Estrutura das pastas do app
```
desafio-front-kelvin/
  layout/
  node_modules/
  public/
  src/
    actions/
    assets/
      images/
    components/
    constant/
    reducers/
    screens/
    store/
    utils/
```

### `heroku`
[https://heroes-of-marvel.herokuapp.com/](https://heroes-of-marvel.herokuapp.com/)

### `npm i`
Para instalar todos os pacotes

### `npm start`
Para iniciar o projeto
[http://localhost:3000](http://localhost:3000)

