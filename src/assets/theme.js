import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
        primary: {
            contrastText: '#FFFFFF',
            light: '#775957a1',
            main: '#4f1313',
        },
        secondary: {
            main: '#FFF'
        },
        text: {
            primary: '#FFF'
        }
    },
    typography: {
        useNextVariants: true,
    },
    overrides: {
        MuiDialog: {
            paperWidthSm: {
                maxWidth: '100%',
            },
            paperWidthMd: {
                maxWidth: '100%',
            },
            paperWidthLg: {
                maxWidth: '100%',
            }
        }
    }
})

export default theme