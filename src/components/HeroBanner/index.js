import React from 'react';
import PropTypes from 'prop-types';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    banner: {           
        height: 200,
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        width: '100%'
     
    },
}));

const HeroBanner = ({ hero }) => {

    const classes = useStyles();

    return (
       <div 
            style ={ { backgroundImage: `url(${ hero.thumbnail.path }.${hero.thumbnail.extension })` }}
            className={ classes.banner }
        /> 
    );
}

HeroBanner.prototype = {
    hero: PropTypes.object.isRequired
}

export default HeroBanner;