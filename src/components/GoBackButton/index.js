import React from 'react';
import PropTypes from 'prop-types';

import ButtonBase from '@material-ui/core/ButtonBase';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        backgroundColor: '#0000008f',
        height: 60,
        flexDirection: 'row',
        justifyContent: 'start',
        zIndex: 1,
        width: '100%',
        position: 'absolute',
        left: 0,
        top: 80,
    },
    icon: {
        color: theme.palette.secondary.main,
        marginLeft: 20,
        marginRight: 20,
        fontSize: 40
    }
}));

const GoBackButton = ({ onClick, text }) => {

    const classes = useStyles();

    return (
        <ButtonBase onClick={onClick} className={ classes.root }>
            <ArrowBackIcon className={ classes.icon }/>
            <Typography variant='h4' color="textPrimary">{text}</Typography>
        </ButtonBase>
    );
}

GoBackButton.prototype = {
    onClick: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired
}

export default GoBackButton;