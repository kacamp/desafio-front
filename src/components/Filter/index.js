import React from 'react';
import PropTypes from 'prop-types';
import { isEmpty, uniqBy } from 'lodash';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { useTheme, makeStyles } from '@material-ui/core/styles';
import FilterIcon from '@material-ui/icons/FilterList'

import BackgroundImage from '../../assets/image/background.jpg';
import FilterChip from '../../components/FilterChip';


const useStyles = makeStyles(theme => ({
    image: {
        position: 'relative',
        zIndex: 2,
        '&:after': {
            backgroundImage: `url(${BackgroundImage})`,
            content: '""',
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            opacity: 0.3,
            position: 'absolute',
            zIndex: -1,
        }
    },
    primary: {
        color: theme.palette.primary.main
    },
    content: {
        backgroundColor: theme.palette.secondary.main,
        [theme.breakpoints.up('md')]: {
            maxHeight: 500,
        }
    },
    formControl: {
        margin: theme.spacing(3),
    },
    empty: {
        marginTop: 4
    },
    rightIcon: {
        marginLeft: theme.spacing(1),
    },
}));

const Filter = ({ text, types, data, onFilter }) => {

    const [open, setOpen] = React.useState(false);

    const [dataFilter, setData ] = React.useState([]);

    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
    const classes = useStyles();

    const [state, setState] = React.useState(types.reduce((acc, cur) => {
        acc[cur.value] = false
        return acc;
    }, {}));

    const handleChange = name => event => {
       
        setState({ ...state, [name]: event.target.checked });
        
        if (event.target.checked) {

            const items = uniqBy([...data.map(d =>  d[name].map(_item => _item.name) ).reduce((acc, cur) => {
                acc = [ ...acc, ...cur]
                return acc;
            }, []).map((_item) => ({ value: name, label: _item, checked: false })), ...dataFilter], item => item.label);
           
            setData([...items]);

        } else {

            const items = dataFilter.filter((_item) => _item.value !== name);
            
            setData([...items]);
        }

    };

    const handleChangeNivel2 = item => event => {
        setData([...dataFilter.map((_d) => {
            if (_d.value === item.value && _d.label === item.label) {
                return {..._d, checked: event.target.checked }
            }
            return _d
        })])
    }

    const handleClear = () => {
        setState(types.reduce((acc, cur) => {
            acc[cur.value] = false
            return acc;
        }, {}))
        setData([])
    }
    
    function handleClickOpen() {
        setOpen(true);
    }
    
    function handleClose() {
        setOpen(false);
    }
    
    return (
        <div style={{ margin: 20}}>
            <Button variant="contained" color="primary" onClick={handleClickOpen}>
                { text }
                <FilterIcon className={ classes.rightIcon }/>
            </Button>
            <FilterChip 
                onDelete={(item) => {
                    const items = dataFilter.map((_item) => {
                        if (_item.label === item.label) {
                           return {..._item, checked: false }
                        } else {
                            return _item
                        }
                    });
                    setData([...items]);
                  
                    onFilter({ values: items.filter(_data => _data.checked), type: 'name' });
                    
                }} 
                data={dataFilter.filter(_data => _data.checked)}/>
            <Dialog
                fullScreen={fullScreen}
                open={open}
                aria-labelledby="responsive-dialog-title"
                onClose={handleClose}
                classes={{
                    paper: classes.image,

                }}
            >
                <DialogTitle id="responsive-dialog-title" className={  classes.primary } onClose={handleClose}>
                    { text }
                </DialogTitle>
                <DialogContent className={  classes.content }>
                    <div style={{ flex: 1, flexDirection: 'row' }}>
                        <FormControl component="fieldset" className={classes.formControl}>
                            <FormLabel component="legend">TYPE</FormLabel>
                            <FormGroup className={ classes.primary }>
                                { types.map(type => {
                                    return (
                                        <FormControlLabel
                                            control={
                                                <Checkbox 
                                                    checked={state[type.value]} 
                                                    onChange={handleChange(type.value)} 
                                                    value={type.value}  
                                                    color='primary'
                                                    inputProps={{ 'aria-label': 'primary checkbox' }}
                                                />
                                            }
                                            label={ type.label }
                                            key={type.label}
                                        />
                                    )
                                })}
                            </FormGroup>
                        </FormControl>
                        { isEmpty(dataFilter) ? void 0 : (  
                            <FormControl component="fieldset" className={classes.formControl}>
                                <FormLabel component="legend">NAME</FormLabel>
                                <FormGroup className={ classes.primary }>
                                    { dataFilter.map(names => {
                                        return (
                                            <FormControlLabel
                                                control={
                                                    <Checkbox 
                                                        checked={names.checked} 
                                                        onChange={handleChangeNivel2(names)} 
                                                        value={names.value}  
                                                        color='primary'
                                                        inputProps={{ 'aria-label': 'primary checkbox' }}
                                                    />
                                                }
                                                label={ names.label }
                                                key={names.label}
                                            />
                                        )
                                    })}
                                </FormGroup>
                            </FormControl>
                        )}
                    </div>
                </DialogContent>
                <DialogActions>
                <Button onClick={handleClear} variant='contained' color="secondary">
                    Clear
                </Button>
                <Button 
                    onClick={() => {
                        if (isEmpty( dataFilter.filter(_data => _data.checked))) {
                            onFilter({ values: types.filter(type => state[type.value]), type: 'types'});
                        } else {
                            onFilter({ values: dataFilter.filter(_data => _data.checked), type: 'name' });
                        }
                        handleClose()
                    }}
                    variant='contained' color="primary" autoFocus>
                    Filter
                </Button>
                </DialogActions>
            </Dialog>
        </div>
    )

} 

Filter.prototype = {
    text: PropTypes.string.isRequired,
    types: PropTypes.array.isRequired,
    data: PropTypes.array.isRequired,
    onFilter: PropTypes.array.isRequired,
}

export default Filter;