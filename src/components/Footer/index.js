import React from 'react';

import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles({
    root: {
        backgroundColor: '#000',
        padding: 10,
        textAlign: 'center',
        display: 'flex',
        flexShrink: 0,
        justifyContent: 'center',
    },
});

const Footer = () => {

    const classes = useStyles();

    return (
        <div className={ classes.root }>
            <Typography variant="caption" color='textPrimary'>
               kacampagnolli@gmail.com
            </Typography>
        </div>
    );
}


export default Footer;