
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress  from '@material-ui/core/CircularProgress';

const useStyles = makeStyles( theme => ({
    loading: {
        display: 'flex',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    margin: {
        margin: theme.spacing(1),
    },
}));


const Loading = () => { 

    const classes = useStyles();

    return (
        <div className={ classes.loading }>
            <CircularProgress color='primary' size={ 56 } className={ classes.margin }/>
        </div>
    )
}

export default Loading;