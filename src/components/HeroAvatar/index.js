import React from 'react';
import PropTypes from 'prop-types';

import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';


const useStyles = makeStyles(theme => ({
    avatar: {
        border: `solid 10px ${ theme.palette.primary.main } `,
        [theme.breakpoints.down('md')]: {
            width: 120,
            height: 120,
            alignSelf: 'center',
            
        },
        [theme.breakpoints.up('md')]: {
            width: 180,
            height: 180,
            alignSelf: 'flex-start',
        },
    },
    container: {
        display: 'flex', 
        [theme.breakpoints.down('md')]: {
            flexDirection: 'column',
            alignItems: 'center'
        },
        [theme.breakpoints.up('md')]: {
            flexDirection: 'row'
        },
        marginBottom: 10
    },
    info: {
        display: 'flex', 
        flexDirection: 'column',
        [theme.breakpoints.down('md')]: {
            alignItems: 'center'
        },
        [theme.breakpoints.up('md')]: {
            marginLeft: 10,
            alignSelf: 'flex-end',
        },
    }
}));

const HeroAvatar = ({  hero }) => {

    const classes = useStyles();

    return (
        <div className={ classes.container }>
            <Avatar  alt={ hero.name } src={`${ hero.thumbnail.path }.${ hero.thumbnail.extension }`} className={ classes.avatar } />
            <div className={ classes.info }>
                <Typography color='primary' variant='h4'>
                    { hero.name }
                </Typography>
                <Typography color='primary' variant='h4'>
                    ID: { hero.id }
                </Typography>
            </div>
        </div> 
    );
}

HeroAvatar.prototype = {
    hero: PropTypes.object.isRequired
}

export default HeroAvatar;