import React from 'react';
import PropTypes from 'prop-types';
import { isEmpty } from 'lodash';

import { makeStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import { NOT_FOUND } from '../../constants';


const useStyles = makeStyles(theme => ({
    painel: {
        maxWidth: 640,
        backgroundColor: 'transparent'
    },
    root: {
        display: 'flex',
        flexDirection: 'column',
        padding: '8px 24px 24px',
        backgroundColor: theme.palette.primary.light,
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        flexBasis: '33.33%',
        flexShrink: 0,
    },
    summary: {
        backgroundColor: theme.palette.primary.main
    }
}));

const HeroPanel = ({ text, data }) => {

    const classes = useStyles();

    return (
        <ExpansionPanel className={ classes.painel }>
            <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon style={{ color: '#FFF' }} />}
                aria-controls="panel1bh-content"
                id="panel1bh-header"
                className={classes.summary}
            >
                <Typography className={classes.heading}>{text}</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails  classes={{ root:  classes.root }}>
                {!isEmpty(data) ? data.map(_d => (
                    <Typography key={`${text}-${_d.name}`} >
                        - { _d.name}
                    </Typography>
                )) : (
                    <Typography >
                       { text } { NOT_FOUND }
                    </Typography>
                )}
            </ExpansionPanelDetails>
      </ExpansionPanel>
    );
}

HeroPanel.prototype = {
    text: PropTypes.string.isRequired,
    data: PropTypes.array.isRequired
}

export default HeroPanel;