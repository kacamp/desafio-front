import React, { Component } from 'react';

import { compose } from 'recompose';

import AppBar from '@material-ui/core/AppBar';
import { withStyles } from '@material-ui/core/styles';
import withWidth from '@material-ui/core/withWidth';

import BackgroundImage from '../../assets/image/background.jpg';

import { containerWidths } from '../../utils';
import { IMG_SITE_BLINDADO, IMG_MARVEL } from '../../constants';

class Scaffold extends Component {

    render() {

        const { classes, width } = this.props;

        return (
            <div className={ classes.root }>
                <div className={ classes.content }>
                    <AppBar position='absolute' className={ classes.appBar }>
                        <img src={ IMG_SITE_BLINDADO } className={ classes.logo } alt='Site Blindado'/>
                        { width !== 'xs' ? (
                            <img src={ IMG_MARVEL } className={ classes.marvelLogo } alt='Marvel'/>
                        ) : void 0 }
                    </AppBar>
                    <main className={ classes.main }>
                        { this.props.children }
                    </main>
                </div>
            </div>
        )
    }

}

const enhance = compose(
    withWidth(),
    withStyles(theme => ({
        root: {
            "-webkit-flex": '1 1 auto',
           flex: 1, 
           height: 'calc(100% - 40px)'
        },
        appBar: {
            backgroundColor: '#000',
            height: 80,
            paddingLeft: 20,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between'

        },
        content: {
            overflow: 'auto',
            display: 'flex',
            flexGrow: 1,
            height: '100%',
            zIndex: 1,
            flexDirection: 'column',
            alignItems: 'center',
            '&:after': {
                backgroundImage: `url(${BackgroundImage})`,
                content: '""',
                height: '100%',
                opacity: 0.3,
                position: 'absolute',
                width: '100%',
                zIndex: -1,
            }
        },
        logo: {
            width: 200,
            height: 80
        },
        marvelLogo: {
            marginTop: 25,
            width: 220,
            height: 104,
            zIndex: 2
        },
        main: {
            marginTop: 80,
            flex: 1,
            display: 'flex',
            height: 'calc(100% - 40px)',
            justifyContent: 'center',
            ...(containerWidths(theme))
        }
    }))
) 

export default enhance(Scaffold);