import React from 'react';
import PropTypes from 'prop-types';
import { isEmpty } from 'lodash';

import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import FilterIcon from '@material-ui/icons/FilterList'
import Tooltip from '@material-ui/core/Tooltip';


const useStyles = makeStyles(({
    card: {
        width: 240,
        backgroundColor: '#0000008f',
        margin: 20,
        position: 'relative'
    },
    media: {
        height: 160,
    },
    infoFilter: {
        position: 'absolute',
        display: 'flex',
        width: 240,
        flexDirection: 'row-reverse',
        zIndex: 1
    }
}));

const HeroCard = ({ onClick, hero, filter }) => {

    const classes = useStyles();

    return (
        <Card className={classes.card}>
            {!isEmpty(filter) ? (
                <div className={ classes.infoFilter }>
                    <Tooltip title={filter.reduce((acc, cur) => {
                        if (hero[cur.value].items.some(item => item.name === cur.label)) {
                            if (acc === '') {
                                acc = acc.concat(cur.label);
                            } else {
                                acc = acc.concat(`, ${cur.label}`);
                            }
                            return acc;
                        }
                        return acc;
                    },'')} placement="right-start">
                        <FilterIcon color='primary' />
                    </Tooltip> 
                </div>
            ) : void 0}
             <CardActionArea onClick={onClick} >
                <CardMedia
                    className={classes.media}
                    image={`${hero.thumbnail.path}.${hero.thumbnail.extension}`}
                    title={hero.name}
                />
                <CardContent>
                    <Typography gutterBottom variant="h6">
                       {hero.name}
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    );
}

HeroCard.prototype = {
    onClick: PropTypes.func.isRequired,
    hero: PropTypes.object.isRequired,
    filter: PropTypes.array.isRequired
}

export default HeroCard;