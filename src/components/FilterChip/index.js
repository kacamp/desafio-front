import React from 'react';
import PropTypes from 'prop-types';

import { makeStyles } from '@material-ui/core/styles';
import Chip from '@material-ui/core/Chip';


const useStyles = makeStyles(theme => ({
    content: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'center',
        marginTop: 10
    },
    chip: {
        margin: theme.spacing(1),
       
    },
    wrap: {
        whiteSpace: 'normal'
    }
}));

const FilterChip = ({ onDelete, data }) => {

    const classes = useStyles();
    return (
        <div className={ classes.content }>
            {data.map((_d) => {
                return (
                    <Chip
                        label={_d.label}
                        onDelete={() => onDelete(_d)}
                        className={ classes.chip }
                        color="primary"
                        key={_d.label}
                        classes={{
                            label: classes.wrap
                        }}
                    />
                )
            })}
        </div>
    );
}

FilterChip.prototype = {
    onDelete: PropTypes.func.isRequired,
    data: PropTypes.string.isRequired
}

export default FilterChip;