import md5 from 'js-md5';


const PUBLIC_KEY = '764a1b63f94a1322fdc85c2e0b2b670a';
const PRIVATE_KEY = '47e7647b130a8782ec5aef539853dca90ff0f169'

export function generateMarvelRequest() {
    const timestamp = Number(new Date())
    const hash = md5.create()
    hash.update(timestamp + PRIVATE_KEY + PUBLIC_KEY)

    return { ts: timestamp, apikey: PUBLIC_KEY, hash }
}


export const tileUnit = 160;

export const containerWidths = (theme) => ({
    [theme.breakpoints.down('sm')]: {
        width: '100%'
    },
    [theme.breakpoints.between('sm', 'sm')]: {
        width: tileUnit * 4
    },
    [theme.breakpoints.between('md', 'md')]: {
        width: tileUnit * 6
    },
    [theme.breakpoints.up('lg')]: {
        width: tileUnit * 7
    }
});

