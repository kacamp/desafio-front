import { LOAD_HERO_SUCCESS, LOAD_HERO_ERROR, LOAD_HERO_LOADING } from './actionTypes';
import axios from 'axios';

import  { generateMarvelRequest } from '../utils'

export const loadHeroes = () => async dispatch => {
    
    try {

        dispatch(loadHeroLoading());

        const marvelRequest = generateMarvelRequest();

        const { data } = await axios.get(`https://gateway.marvel.com:443/v1/public/characters?ts=${marvelRequest.ts}&apikey=${marvelRequest.apikey}&hash=${marvelRequest.hash}`);
        dispatch(loadHeroSuccess(data.data.results));

    } catch (err) {

        dispatch(loadHeroError(err.message));

    }

};

const loadHeroSuccess = heroes => ({
    type: LOAD_HERO_SUCCESS,
    payload: {
      heroes
    }
});
  
const loadHeroLoading = () => ({
    type: LOAD_HERO_LOADING
});
  
const loadHeroError = error => ({
    type: LOAD_HERO_ERROR,
    payload: {
        error
    }
});