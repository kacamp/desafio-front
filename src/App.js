import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Scaffold from './components/Scaffold';

import Detail from './screens/Detail';
import Home from './screens/Home';
import NotFound from './screens/NotFound';

function App() {
  	return (
   		<Scaffold>
			<Switch>
				<Route path='/' exact component={ Home }/>
				<Route path='/:id' exact component={ Detail }/>
				<Route component={ NotFound } />
			</Switch>
   		</Scaffold>
  	);
}

export default App;
