import { LOAD_HERO_SUCCESS, LOAD_HERO_ERROR, LOAD_HERO_LOADING } from '../actions/actionTypes';

const initialState = {
    heroList: [],
    error:'',
    loading: false,
};

const heroReducer = ( state = initialState, action) => {
    switch (action.type) {
        case LOAD_HERO_LOADING: {
            return {
                ...state,
                loading: true,
                error: ''
            };
        }
        case LOAD_HERO_SUCCESS: {
            return {
                ...state,
                heroList: [...state.heroList, ...action.payload.heroes],
                loading: false
            };
        }
        case LOAD_HERO_ERROR: {
            return {
                ...state,
                error: action.payload.error,
                loading: false
            };
        }
        default: 
            return state;
    }
}

export default heroReducer