export const IMG_MARVEL = require('../assets/image/MarvelLogo.svg')
export const IMG_SITE_BLINDADO = require('../assets/image/logo.png');

export const COMICS = 'COMICS';
export const STORIES = 'STORIES';
export const EVENTS = 'EVENTS';
export const SERIES = 'SERIES';
export const ALL_CHARACTER = 'All Character';
export const NOT_FOUND = 'NOT FOUND';
export const DESCRIPTION = 'DESCRIPTION';
export const PAGE = 'PAGE';

export const TYPES = [
    { value: 'comics', label: COMICS },
    { value: 'events', label: EVENTS },
    { value: 'series', label: SERIES },
]