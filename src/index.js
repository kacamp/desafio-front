import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import Store  from './store';

import { MuiThemeProvider } from '@material-ui/core';

import theme from './assets/theme';
import Footer from './components/Footer';


ReactDOM.render(
    <MuiThemeProvider theme={theme}>
        <Provider store={Store}>
            <BrowserRouter>
                <>
                    <App />
                    <Footer/>
                </>
            </BrowserRouter>
        </Provider>
    </MuiThemeProvider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
