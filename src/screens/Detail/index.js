import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import { isEmpty } from 'lodash';
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';

import GoBackButton from '../../components/GoBackButton';
import HeroPanel from '../../components/HeroPanel';
import HeroAvatar from '../../components/HeroAvatar';
import HeroBanner from '../../components/HeroBanner';
import Loading from '../../components/Loading';

import { COMICS, EVENTS, SERIES, STORIES, ALL_CHARACTER, NOT_FOUND, DESCRIPTION } from '../../constants';

class Detail extends Component {

    constructor(props) {
        
        super(props);

        this.state = {
            selectedHero: null
        };

    } 

    componentDidMount() {
     
        const selectedHero = this.props.heroList.find(hero => hero.id === Number(this.props.match.params.id));
        if (selectedHero) {
            this.setState({ selectedHero });
        } else {
            this.props.history.push('/');

        }

    }

    render() {
        const { history, classes } = this.props;
        const { selectedHero } = this.state;
        if (selectedHero) {
            return (
                <div style={{ flex: 1}}>
                    <GoBackButton text={ ALL_CHARACTER } onClick={() => history.push('/')}/>
                    <HeroBanner hero={selectedHero}/>
                    <div className={ classes.content }>
                        <div className={ classes.info } style={{ position: 'relative', top: -80}}>
                            <HeroAvatar hero={selectedHero} /> 
                            <HeroPanel text={ COMICS } data={selectedHero.comics.items.map(c => ({ name: c.name, url: c.resourceURI }))} />
                            <HeroPanel text={ STORIES } data={selectedHero.stories.items.map(s => ({ name: s.name, url: s.resourceURI }))} />
                            <HeroPanel text={ EVENTS } data={selectedHero.events.items.map(e => ({ name: e.name, url: e.resourceURI }))} />
                            <HeroPanel text={ SERIES } data={selectedHero.series.items.map(s => ({ name: s.name, url: s.resourceURI }))} />
                        </div>
                        <div className={ classes.info }>
                            <div className={ classes.description }>
                                <Typography color='textPrimary' variant='h6' style={{ lineHeight: 2.0 }}>
                                    { !isEmpty(selectedHero.description) ? selectedHero.description : `${ DESCRIPTION } ${ NOT_FOUND }` }
                                </Typography>
                            </div>
                        </div>
                    </div>
                </div>
              
            )
        } else {
            return  <Loading/>
        }
    }

}

const mapStateToProps = state => {
    return {
      heroList: state.heroList,
    };
};

const enhance = compose(
    withRouter,
    connect(mapStateToProps),
    withStyles(theme => ({
        content: {
            display: 'flex',
            flexDirection: 'row',
            [theme.breakpoints.down('md')]: {
                flexDirection: 'column',
            },
            [theme.breakpoints.up('md')]: {
                flexDirection: 'row',
            },
            justifyContent: 'center',
            width: '100%'
        },
        info: {
            display: 'flex',
            flexDirection: 'column',
            flex: 1,
        },
        description: {
            backgroundColor: theme.palette.primary.light, 
            [theme.breakpoints.up('md')]: {
                marginLeft: 20 
            },
            padding: 20, 
            marginTop: 50 
        }
    }))
)

export default enhance(Detail)