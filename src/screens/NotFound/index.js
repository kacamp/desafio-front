import React, { Component } from 'react';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';

import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

import GoBackButton from '../../components/GoBackButton';
import { NOT_FOUND, PAGE } from '../../constants';

class NotFound extends Component {

    render() {

        const { classes, history } = this.props;

        return (
            <div className={ classes.root }>
                <GoBackButton text='All Character' onClick={() => history.push('/')}/>
                <div className={ classes.content }>
                    <Typography variant='h1' color='primary' >{ PAGE } { NOT_FOUND } :(</Typography>
                </div>
            </div>
        );
    }

}

const enhance = compose(
    withRouter,
    withStyles(theme => ({
        content: {
            display: 'flex',
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center'
        },
        root: {
            display: 'flex',
            flexDirection: 'column',
            flex: 1
        }
    }))
)

export default enhance(NotFound);
