import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';

import { loadHeroes } from '../../actions'

import HeroCard from '../../components/HeroCard';
import Loading from '../../components/Loading';
import Filter from '../../components/Filter';


import { withStyles } from '@material-ui/core/styles'
import { TYPES } from '../../constants';



class Home extends Component {

    constructor(props) {
        
        super(props);

        this.state = {
            filterList: [],
            filters: [],
        }

    }

    componentDidMount() {
        if (isEmpty(this.props.heroList)) {
            this.props.loadHeroes(this.props.offset);
        }
    }

    render() {

        const { heroList, loadingHero, classes, history } = this.props;

        if (isEmpty(heroList) && loadingHero) {
            return  <Loading/>
        }
        return (

            <div>
                <Filter 
                    text='Filter' 
                    types={ TYPES } 
                    data={
                        heroList.map(hero => ({ 
                            hero: hero.id, comics: hero.comics.items, events: hero.events.items, series: hero.series.items 
                        }))
                    }
                    onFilter={(dataFilter) => {
                        this.setState({ filters: dataFilter, filterList: heroList.filter( hero => {
                            if (dataFilter.type === 'name') {
                                return dataFilter.values.some( f => hero[f.value].items.some(item => item.name === f.label))
                            } else {
                                return dataFilter.values.some( f => hero[f.value].items.length > 0);
                            }
                        })});
                    }}
                />
               
                <div>

                </div>
                <div className={ classes.content } >
                    { (isEmpty(this.state.filterList) ? heroList : this.state.filterList ).map(hero => (
                        <HeroCard onClick={() => history.push(`/${hero.id}`)} hero={ hero } key={ hero.id } filter={ this.state.filters.type === 'name' ? this.state.filters.values : [] }/>
                    ))}
                </div>
                { loadingHero ? (
                   <Loading/>
                ): void 0 }
            </div>
            
        )
    }
}


const mapStateToProps = state => {
    return {
        heroList: state.heroList,
        loadingHero: state.loading,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        loadHeroes: () => {
            dispatch(loadHeroes());
        }
    };
};
  
const enhance = compose(
    withRouter,
    withStyles(theme => ({
        content: {
            display: 'flex',
            flexDirection: 'row',
            flexWrap: 'wrap',
            justifyContent: 'center'
        },
        margin: {
            margin: theme.spacing(1),
        },
    })),
    connect(mapStateToProps, mapDispatchToProps)
)

export default enhance(Home)